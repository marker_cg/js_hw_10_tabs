const tabs = document.querySelectorAll('.tabs-title');
const tabContent = document.querySelectorAll('.tab-content');



tabs.forEach(tab => {
    tab.addEventListener('click', openActiveTab);
});

function openActiveTab(tab) {
    let activeTab = tab.currentTarget;
    let about = activeTab.dataset.about

    tabs.forEach(tab => {
        tab.classList.remove('active')
    });

    tabContent.forEach(tab => {
        tab.classList.remove("active");
     });

    document.querySelector("#" + about).classList.add("active");

    activeTab.classList.add('active')
}